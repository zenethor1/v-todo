import React, {Component} from 'react';
import {Switch, Route} from 'react-router';
import Home from './pages/Home';
import About from './pages/About';
import Page404 from './pages/Page404';

class App extends Component<any, any> {

  public state = {};

  public render() {
    return (
      <div>
        <div>Best TODO app ever</div>
        <Switch>
          <Route exact path="/" component={Home}/>
          <Route path="/about" component={About}/>
          <Route component={Page404}/>
        </Switch>
      </div>
    );
  }
}

export default App;
